import axios from 'axios';

export default class DirectoryInfoApi {
    static getDirectoriesInfo(directoryPath) {
        return axios.get('http://localhost:5000/api/directories', {
            params: {
                sourceFolderPath: directoryPath
            },
        }).then(response => response.data,
            error => {
                throw error;
            });
    }
}
