import React, { Component } from 'react'
import { Typography, Grid } from '@material-ui/core'
import nanoid from 'nanoid';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  heading: {
    fontSize: '1.2em',
    fontWeight: 'bold',
    color: 'black',
    paddingBottom: theme.spacing.unit,
  },
  container: {
    paddingTop: theme.spacing.unit,
  },
  row: {
    paddingBottom: theme.spacing.unit / 2,
    paddingTop: theme.spacing.unit / 2,
  }
})

export class DeniedDirectories extends Component {
   
      renderNoDirectoriesRow = () => {
        return (
          <Typography variant="body1">
            No directories
          </Typography>
        );
      }
    
      renderDirectories = (directories) => {
        const { classes } = this.props;
        return directories.map(directory => {
          return (
            <Grid container className={classes.row} key={nanoid()} >
              <Grid item>
                {directory.name}
              </Grid>
            </Grid>
          )
        });
      }
  render() {
    const { deniedDirectories, classes } = this.props;

    return (
      <Grid container className={classes.container}>
        {deniedDirectories && deniedDirectories.length > 0
          ? this.renderDirectories(deniedDirectories)
          : this.renderNoDirectoriesRow()}
      </Grid>
    )
  }
}

export default withStyles(styles)(DeniedDirectories)
