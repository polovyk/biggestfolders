import React, { Component } from 'react'
import { Typography, Grid } from '@material-ui/core'
import nanoid from 'nanoid';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  heading: {
    fontSize: '1.2em',
    fontWeight: 'bold',
    color: 'black',
    paddingBottom: theme.spacing.unit,
  },
  container: {
    paddingTop: theme.spacing.unit,
  },
  row: {
    paddingBottom: theme.spacing.unit / 2,
    paddingTop: theme.spacing.unit / 2,
  }
})

export class DirectoriesList extends Component {

  renderHeading = () => {
    const { classes } = this.props;
    return (
      <Grid container className={classes.heading} key={nanoid()}>
        <Grid item xs={3}>
          Name
        </Grid>
        <Grid item xs={1}>
          Size
        </Grid>
        <Grid item>
          Unit
        </Grid>
      </Grid>
    );
  }

  renderNoDirectoriesRow = () => {
    return (
      <Typography variant="body1">
        No directories
      </Typography>
    );
  }

  renderDirectories = (directories) => {
    const { classes } = this.props;
    return directories.map(directory => {
      return (
        <Grid container className={classes.row} key={nanoid()} >
          <Grid item xs={3}>
            {directory.name}
          </Grid>
          <Grid item xs={1}>
            {directory.size}
          </Grid>
          <Grid item>
            {directory.sizeUnit}
          </Grid>
        </Grid>
      )
    });
  }

  render() {
    const { avaliableDirectories, classes } = this.props;

    return (
      <Grid container className={classes.container}>
        {this.renderHeading()}
        {avaliableDirectories && avaliableDirectories.length > 0
          ? this.renderDirectories(avaliableDirectories)
          : this.renderNoDirectoriesRow()}
      </Grid>
    )
  }
}

export default withStyles(styles)(DirectoriesList)
