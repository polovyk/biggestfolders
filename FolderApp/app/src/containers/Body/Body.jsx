import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Button, TextField, Grid, Typography, CircularProgress } from '@material-ui/core'
import DirectoriesList from '../../components/DirectoriesList'
import DeniedDirectories from '../../components/DeniedDirectories'
import { getDirectoriesInfo } from '../../redux/modules/directoryInfo';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    container: {
        marginLeft: theme.spacing.unit * 2,
        marginRight: theme.spacing.unit * 2,
    },
    searchInput: {
        marginRight: theme.spacing.unit / 2,
    },
    areaBlock: {
        marginBottom: theme.spacing.unit,
    },
    progress: {
        marginLeft: theme.spacing.unit,
    }
})

function mapStateToProps(state) {
    return {
        avaliableDirectories: state.directoryInfo.avaliableDirectories,
        deniedDirectories: state.directoryInfo.deniedDirectories
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getDirectoriesInfo: directoryPath => dispatch(getDirectoriesInfo(directoryPath)),
    };
}

export class Body extends Component {
    constructor(props) {
        super(props);
        this.state = {
            directoryPath: '',
            isLoading: false
        };
    }

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    searchDirectories = () => {
        const { directoryPath } = this.state;
        const { getDirectoriesInfo } = this.props;
        this.setState({ isLoading: true });
        getDirectoriesInfo(directoryPath).then(() => {
            this.setState({ isLoading: false });
        });
    }

    render() {
        const { avaliableDirectories, deniedDirectories, classes } = this.props;
        const { isLoading } = this.state;
        return (
            <div className={classes.container}>
                <Grid container alignItems='center' className={classes.areaBlock}>
                    <Grid item>
                        <TextField
                            value={this.state.directoryPath}
                            name="directoryPath"
                            onChange={this.handleChange}
                            label="Source directory path"
                            required={true}
                            className={classes.searchInput}
                            placeholder="C:\"
                            disabled={isLoading}
                        />
                    </Grid>
                    <Grid item>
                        <Button onClick={this.searchDirectories} variant="contained" color="primary" disabled={isLoading}>Search</Button>
                    </Grid>
                    <Grid item>
                        {isLoading && (<CircularProgress className={classes.progress} />)}
                    </Grid>
                </Grid>
                <Grid className={classes.areaBlock}>
                    <Typography variant="h5">Search results</Typography>
                    <DirectoriesList avaliableDirectories={avaliableDirectories} />
                </Grid>
                {deniedDirectories && deniedDirectories.length > 0 && (
                    <Grid className={classes.areaBlock}>
                        <Typography variant="h5">Missing permissions to folder or its subfolders</Typography>
                        <DeniedDirectories deniedDirectories={deniedDirectories} />
                    </Grid>
                )}
            </div>
        )
    }
}

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Body));
