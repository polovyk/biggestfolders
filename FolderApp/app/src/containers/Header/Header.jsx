import React, { Component } from 'react'
import { AppBar, Toolbar, Typography } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles';
import { blue } from '@material-ui/core/colors';

const styles = theme => ({
    container: {
        marginBottom: theme.spacing.unit * 2,
        backgroundColor: blue[500]
    },
});

class Header extends Component {
    render() {
        const { classes } = this.props;

        return (
            <AppBar position='static' className={classes.container}>
                <Toolbar >
                    <Typography variant="h6" color="secondary">
                        Biggest folders
                    </Typography>
                </Toolbar>
            </AppBar>
        );
    };
}

export default withStyles(styles)(Header)