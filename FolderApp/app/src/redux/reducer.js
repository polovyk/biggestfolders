import { combineReducers } from 'redux';
import directoryInfo from './modules/directoryInfo';

export default combineReducers({
    directoryInfo,
});
