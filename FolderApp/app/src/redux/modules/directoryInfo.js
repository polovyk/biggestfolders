import DirectoryInfoApi from '../../api/directoryInfo';
import * as constants from '../../redux/actionTypes/directoryInfo';

const initState = {
    avaliableDirectories: [],
    deniedDirectories: []
}

export default function reducer(state = initState, action) {
    switch (action.type) {
        case constants.GET_DIRECTORIES_INFO_SUCCESS: {
            const { avaliableDirectories, deniedDirectories } = action.responseData;
            return { ...state, avaliableDirectories, deniedDirectories };
        }
        case constants.GET_DIRECTORIES_INFO_FAILURE: {
            return { 
                ...state,
                avaliableDirectories: initState.avaliableDirectories, 
                deniedDirectories: initState.deniedDirectories };
        }
        default:
            return state;            
    }
}

export function getDirectoriesInfo(directoryPath) {
    return dispatch => {
      dispatch({
        type: constants.GET_DIRECTORIES_INFO_REQUEST
      })
  
      return DirectoryInfoApi.getDirectoriesInfo(directoryPath).then(
        responseData => {
          dispatch({
            type: constants.GET_DIRECTORIES_INFO_SUCCESS,
            responseData
          })
        },
        error => {
          dispatch({
            type: constants.GET_DIRECTORIES_INFO_FAILURE
          });
          throw error;
        }
      )
    }
  }