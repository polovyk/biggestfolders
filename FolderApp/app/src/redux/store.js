
import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import ReduxThunk from 'redux-thunk';
import reducer from './reducer';

const getMiddleware = () => {
  if (process.env.NODE_ENV === 'production') {
    return applyMiddleware(ReduxThunk);
  } else {
    // Enable additional logging in non-production environments and add devtools.
    return composeWithDevTools(applyMiddleware(ReduxThunk, createLogger()));
  }
};

export const store = createStore(
  reducer,
  getMiddleware()
);
