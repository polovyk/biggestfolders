
﻿import { createMuiTheme } from '@material-ui/core/styles';

export const theme = createMuiTheme({
    spacing: {
        unit: 8
    },
    typography: {
        useNextVariants: true,
    },
    overrides: {
        MuiInput: {
            root: {
                '& input': {
                    fontSize: '1em'
                }
            },
        },
        MuiSelect: {
            root: {
                '& select': {
                    fontSize: '1em'
                }
            },
        },
        MuiInputLabel: {
            root: {
                fontSize: '1.2em',
            }
        }
    },
    palette: {
        primary: {
            light: '#587fcc',
            main: '#1c539b',
            dark: '#002b6c',
            contrastText: '#fff',
        },
        secondary: {
            light: '#ffffff',
            main: '#ffffff',
            dark: '#cccccc',
            contrastText: '#000',
        }
    },
})
