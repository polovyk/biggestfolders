﻿using FolderApp.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FolderApp.Models
{
    public class DirectoryViewModel
    {
        public IEnumerable<DirectorySizeInfo> AvaliableDirectories { get; set; }
        public IEnumerable<DeniedDirectoryInfo> DeniedDirectories { get; set; }
    }
}
