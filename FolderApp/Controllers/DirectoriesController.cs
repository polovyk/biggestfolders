﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FolderApp.Core.Services;
using FolderApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace FolderApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DirectoriesController : ControllerBase
    {
        private readonly IDirectoryService _directoryService;

        public DirectoriesController(IDirectoryService directoryService)
        {
            _directoryService = directoryService;
        }

        // GET api/directories
        [HttpGet]
        public async Task<IActionResult> Get(string sourceFolderPath)
        {
            //todo: handle missing or denied folder exception
            const int amountOfFolders = 5;
            var (available, denied) = await _directoryService.GetBiggestFolders(sourceFolderPath, amountOfFolders);

            var model = new DirectoryViewModel
            {
                AvaliableDirectories = available,
                DeniedDirectories = denied
            };

            return new JsonResult(model);
        }
    }
}
