##Description
This project was written using .Net Core for WebAPI and web page on React/Redux. Application was tested on Windows 10


##WebAPI
WebAPI project - FolderApp
WebAPI url is configured on http://localhost:5000.

Allowed CORSes for web page http://localhost:3000


##React page
Page sources - FolderApp/app
Page is expecting API to be hosted on http://localhost:5000. Can be changed in FolderApp/app/src/api/directoryInfo.js

Be sure you have installed all npm dependencies. Use command `npm install` for this. Command can be runnned by cdm from folder 'FolderApp/app'

To host the page you can use script `npm start` 

## Available Scripts
In the project directory, you can run:

### `npm start`
Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`
Builds the app for production to the `build` folder.


During development were used:
##.Net Core 
version 2.1.505
##npm 
version 5.6.0
##NodeJS 
version 8.11.4