﻿using FolderApp.Controllers;
using FolderApp.Core.Services;
using FolderApp.Entites;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FolderApp.Tests
{
    public class DirectoriesControllerTests
    {
        private readonly IEnumerable<DirectorySizeInfo> _directorySize = new List<DirectorySizeInfo>
            {
                new DirectorySizeInfo
                {
                    Name = "Test",
                    Size = 1,
                    SizeBytes = 1,
                    SizeUnit = "Bytes"
                }
            };
        private readonly IEnumerable<DeniedDirectoryInfo> _deniedDirectories = new List<DeniedDirectoryInfo>();

        [Fact]
        public void Contoller_response_should_be_not_null()
        {
            var task = Task.Run(() =>
            {
                return (_directorySize, _deniedDirectories);
            });
            var directoryPath = "T:\\";

            var mock = new Mock<IDirectoryService>();
            mock.Setup(a => a.GetBiggestFolders(directoryPath, 5)).Returns(task);
            var controller = new DirectoriesController(mock.Object);

            var result = controller.Get(directoryPath).Result as JsonResult;

            Assert.NotNull(result.Value);
        }
    }
}
