using FolderApp.Core.Services;
using FolderApp.Entites;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Xunit;

namespace FolderApp.Tests
{
    public class DirectoryServiceTests
    {
        private readonly string _currentDirectoryPath;
        private readonly string _subDirectory5MbPath;
        private readonly string _subDirectory2MbPath;
        private readonly string _subDirectory30KBPath;
        private readonly string _subDirectory4KBPath;
        private readonly string _subDirectory1KBPath;
        private readonly string _subDirectory6BytesPath;
        private readonly string _subDirectoryEmptyPath;
        private readonly IDirectoryService _service = new DirectoryService();
        private readonly int _amountOfFolders = 5;


        public DirectoryServiceTests()
        {
            _currentDirectoryPath = Directory.GetCurrentDirectory();
            _subDirectory5MbPath = $"{_currentDirectoryPath}\\Test5MB";
            _subDirectory2MbPath = $"{_currentDirectoryPath}\\Test2MB";
            _subDirectory30KBPath = $"{_currentDirectoryPath}\\Test30KB";
            _subDirectory4KBPath = $"{_currentDirectoryPath}\\Test4KB";
            _subDirectory1KBPath = $"{_currentDirectoryPath}\\Test1KB";
            _subDirectory6BytesPath = $"{_currentDirectoryPath}\\Test6Bytes";
            _subDirectoryEmptyPath = $"{_currentDirectoryPath}\\TestEmpty";
        }

        [Fact]
        public void Maximum_amount_of_returned_directories_is_5()
        {
            Clear();
            CreateDirectory2Mb();
            CreateDirectory30KB();
            CreateDirectory4KB();
            CreateDirectory1KB();
            CreateDirectory6Bytes();
            CreateDirectoryEmpty();


            var (available, denied) = _service.GetBiggestFolders(_currentDirectoryPath, _amountOfFolders).Result;

            Assert.Equal(available.Count(), _amountOfFolders);
        }

        [Fact]
        public void Amount_of_denied_directories_is_empty()
        {
            Clear();
            CreateDirectory1KB();
            CreateDirectory6Bytes();
            CreateDirectoryEmpty();

            var (available, denied) = _service.GetBiggestFolders(_currentDirectoryPath, _amountOfFolders).Result;

            Assert.Empty(denied);
        }

        [Fact]
        public void Amount_of_allowed_directories_is_empty()
        {
            Clear();

            var (available, denied) = _service.GetBiggestFolders(_currentDirectoryPath, _amountOfFolders).Result;

            Assert.Empty(available);
        }

        [Fact]
        public void Directories_are_ordered_by_size_desc()
        {
            Clear();
            CreateDirectory30KB();
            CreateDirectory6Bytes();
            CreateDirectoryEmpty();
            CreateDirectory5Mb();

            var (available, denied) = _service.GetBiggestFolders(_currentDirectoryPath, _amountOfFolders).Result;
            var directoriesArray = available.ToArray();

            Assert.EndsWith(directoriesArray[0].Name, _subDirectory5MbPath);
            Assert.EndsWith(directoriesArray[1].Name, _subDirectory30KBPath);
            Assert.EndsWith(directoriesArray[2].Name, _subDirectory6BytesPath);
            Assert.EndsWith(directoriesArray[3].Name, _subDirectoryEmptyPath);
        }

        [Fact]
        public void Selecting_only_directories()
        {
            Clear();
            CreateFile(_currentDirectoryPath, 2048);

            var (available, denied) = _service.GetBiggestFolders(_currentDirectoryPath, _amountOfFolders).Result;

            Assert.Empty(available);
        }

        [Fact]
        public void Method_correctly_reads_directory_size()
        {
            Clear();
            CreateDirectory1KB();
            DirectoryInfo directoryInfo = new DirectoryInfo(_subDirectory1KBPath);
            Type type = typeof(DirectoryService);
            MethodInfo method = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance)
            .Where(x => x.Name == "GetDirectorySize" && x.IsPrivate)
            .First();

            var folderSize = (long)method.Invoke(_service, new object[] { directoryInfo });

            Assert.Equal(1024, folderSize);
        }

        private void CreateDirectory5Mb()
        {
            int fileSize = 1024 * 1024 * 5;
            Directory.CreateDirectory(_subDirectory5MbPath);
            CreateFile(_subDirectory5MbPath, fileSize);
        }

        private void CreateDirectory2Mb()
        {
            int fileSize = 1024 * 1024 * 2;
            Directory.CreateDirectory(_subDirectory2MbPath);
            CreateFile(_subDirectory2MbPath, fileSize);
        }

        private void CreateDirectory30KB()
        {
            int fileSize = 1024 * 30;
            Directory.CreateDirectory(_subDirectory30KBPath);
            CreateFile(_subDirectory30KBPath, fileSize);
        }

        private void CreateDirectory4KB()
        {
            int fileSize = 1024 * 4;
            Directory.CreateDirectory(_subDirectory4KBPath);
            CreateFile(_subDirectory4KBPath, fileSize);
        }

        private void CreateDirectory1KB()
        {
            int fileSize = 1024;
            Directory.CreateDirectory(_subDirectory1KBPath);
            CreateFile(_subDirectory1KBPath, fileSize);
        }

        private void CreateDirectory6Bytes()
        {
            int fileSize = 6;
            Directory.CreateDirectory(_subDirectory6BytesPath);
            CreateFile(_subDirectory6BytesPath, fileSize);
        }

        private void CreateDirectoryEmpty()
        {
            Directory.CreateDirectory(_subDirectoryEmptyPath);
        }

        private void CreateFile(string directoryPath, int size)
        {
            string fileName = $"{directoryPath}\\testFile";
            byte[] array = new byte[size];

            using (var stream = File.Create(fileName))
            {
                stream.Write(array);
            }
        }

        private void Clear()
        {
            RemoveTestFolders();
            RemoveTestFile(_currentDirectoryPath);
        }

        private void RemoveTestFolders()
        {
            RemoveTestDirectory(_subDirectory5MbPath);
            RemoveTestDirectory(_subDirectory2MbPath);
            RemoveTestDirectory(_subDirectory30KBPath);
            RemoveTestDirectory(_subDirectory4KBPath);
            RemoveTestDirectory(_subDirectory1KBPath);
            RemoveTestDirectory(_subDirectory6BytesPath);
            RemoveTestDirectory(_subDirectoryEmptyPath);
        }

        private void RemoveTestFile(string directoryPath)
        {
            string fileName = $"{directoryPath}\\testFile";

            if (File.Exists(fileName))
                File.Delete(fileName);
        }

        private void RemoveTestDirectory(string directoryPath)
        {
            RemoveTestFile(directoryPath);

            if (Directory.Exists(directoryPath))
                Directory.Delete(directoryPath);
        }
    }
}
