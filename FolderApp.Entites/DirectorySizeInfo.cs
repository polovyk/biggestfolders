﻿using System;

namespace FolderApp.Entites
{
    public class DirectorySizeInfo
    {
        public string Name { get; set; }

        public long SizeBytes { get; set; }

        public double Size { get; set; }

        public string SizeUnit { get; set; }
    }
}
