﻿using FolderApp.Entites;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FolderApp.Core.Services
{
    public interface IDirectoryService
    {
        Task<(IEnumerable<DirectorySizeInfo>, IEnumerable<DeniedDirectoryInfo>)> GetBiggestFolders(string sourceDirectoryPath, int amountToReturn);
    }
}
