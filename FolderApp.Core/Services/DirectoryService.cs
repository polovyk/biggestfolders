﻿using FolderApp.Entites;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Threading.Tasks;

namespace FolderApp.Core.Services
{
    public class DirectoryService : IDirectoryService
    {
        private const int sizeDivider = 1024;
        private const string Byte = "Byte";
        private const string KB = "KB";
        private const string MB = "MB";
        private const string GB = "GB";

        /// <summary>
        /// Returns the biggest directories and directories without permissions
        /// </summary>
        public async Task<(IEnumerable<DirectorySizeInfo>, IEnumerable<DeniedDirectoryInfo>)> GetBiggestFolders(string sourceDirectoryPath, int amountToReturn)
        {
            var directoriesWithSize = new ConcurrentDictionary<string, long>();
            var deniedDirectories = new ConcurrentDictionary<string, DeniedDirectoryInfo>();
            var tasksList = new List<Task>();
            IEnumerable<DirectorySizeInfo> availableDirectoriesResult = new List<DirectorySizeInfo>();
            IEnumerable<DeniedDirectoryInfo> deniedDirectoriesResult = new List<DeniedDirectoryInfo>();

            if (Directory.Exists(sourceDirectoryPath))
            {
                var sourceDirectoryInfo = new DirectoryInfo(sourceDirectoryPath);
                var subDirectories = sourceDirectoryInfo.GetDirectories();

                //get directory size or mark it as denied
                foreach (var directory in subDirectories)
                {
                    var task = Task.Run(() =>
                    {
                        try
                        {
                            var directoryModel = GetDirectorySize(directory);

                            directoriesWithSize.TryAdd(
                                directory.Name,
                                directoryModel
                            );
                        }
                        catch (SecurityException)
                        {
                            deniedDirectories.TryAdd(
                                directory.Name,
                                new DeniedDirectoryInfo { Name = directory.Name }
                            );
                        }
                        catch (UnauthorizedAccessException)
                        {
                            deniedDirectories.TryAdd(
                                directory.Name,
                                new DeniedDirectoryInfo { Name = directory.Name }
                            );
                        }
                    });

                    tasksList.Add(task);
                }

                await Task.WhenAll(tasksList.ToArray());

                //select required amount of biggest folders and map to model
                availableDirectoriesResult = directoriesWithSize
                    .OrderByDescending(e => e.Value)
                    .Take(amountToReturn)
                    .Select(e => GetDirectorySizeInfo(e.Key, e.Value));

                deniedDirectoriesResult = deniedDirectories.Select(e => e.Value);
            }

            return (availableDirectoriesResult, deniedDirectoriesResult);
        }

        private DirectorySizeInfo GetDirectorySizeInfo(string name, long sizeBytes)
        {
            double size = sizeBytes;
            int counter = 0;
            while (size > sizeDivider)
            {
                counter++;
                size /= sizeDivider;
            }
            size = Math.Round(size, 2);
            string sizeUnit = GetSizeUnit(counter);

            var result = new DirectorySizeInfo
            {
                Name = name,
                SizeBytes = sizeBytes,
                Size = size,
                SizeUnit = sizeUnit
            };

            return result;
        }

        private long GetDirectorySize(DirectoryInfo directoryInfo)
        {
            var totalSize = directoryInfo.EnumerateFiles("*", SearchOption.AllDirectories).Sum(file => file.Length);

            return totalSize;
        }

        private string GetSizeUnit(int dismention)
        {
            switch (dismention)
            {
                case 0:
                    return Byte;
                case 1:
                    return KB;
                case 2:
                    return MB;
                case 3:
                default:
                    return GB;
            }
        }
    }
}
