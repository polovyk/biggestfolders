﻿using FolderApp.Core.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace FolderApp.Core
{
    public static class Bootstrapper
    {
        public static IServiceCollection AddCoreRepositories(this IServiceCollection services)
        {
            return services.AddTransient<IDirectoryService, DirectoryService>();
        }
    }
}
